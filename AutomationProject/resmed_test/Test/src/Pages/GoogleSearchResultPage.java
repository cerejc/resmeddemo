package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Common.Utility;


public class GoogleSearchResultPage {

	WebDriver Driver;

	public GoogleSearchResultPage(WebDriver Driver)
	{
	this.Driver = Driver;	
	}
	
	
	By lnkSearchResult = By.linkText("ResMed");
	
	
	public void ClickSearchResult()
	{
		Driver.findElement(lnkSearchResult).click();
		Utility.PageWait(Driver);
	}
	
	
	
}
