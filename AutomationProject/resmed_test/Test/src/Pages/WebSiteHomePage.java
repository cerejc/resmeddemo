package Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Common.Utility;

public class WebSiteHomePage {
	
	WebDriver Driver;
	By dropdwnUserPreference= By.id("user_pref");
	By eg = By.xpath("//*[@id=\"user_pref\"]/li/ul/li");
		
	public WebSiteHomePage(WebDriver Driver)
	{
		this.Driver = Driver; 
	}
	
	public void verifyWebSiteHomePageisOpened()
	{
		Driver.findElement(dropdwnUserPreference).isDisplayed();
	}
	
	public void clickUserPreferenceDropdwn()
	{
		Driver.findElement(dropdwnUserPreference).click();
	}
	
	public void selectUserPreference ()
	{														
		List<WebElement> choice = Driver.findElements(eg);
		for(WebElement e : choice){
	        //System.out.println(e.getText());
	        String option = "Suppliers";
			if(e.getText().equals(option)){
				Utility.PageWait(Driver); 
	    	    System.out.println("clicking the user preference option as: " + e.getText());
	            e.click();
	            break;
	        }
	    }
	    
	    Utility.PageWait(Driver); 
	}	
}
