package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Common.Utility;

public class GoogleHomePage  {
	

	WebDriver Driver; 
	By txtBxSearch = By.id("lst-ib");
	By btnSearch = By.xpath("//*[@id=\"tsf\"]/div[2]/div[3]/center/input[1]");

	public GoogleHomePage (WebDriver Driver)
	{
		this.Driver = Driver;
	}
	
	public void EnterText()
	{
		Driver.findElement(txtBxSearch).sendKeys("resmed wiki");
	}
	
	public void Click()
	{
		Driver.findElement(btnSearch).click();
		Utility.PageWait(Driver);
		
	}
}
