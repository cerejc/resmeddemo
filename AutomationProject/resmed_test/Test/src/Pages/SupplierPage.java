package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SupplierPage {
	
	WebDriver Driver;
	By txtSupplierPage = By.xpath("//*[@id=\"button-section\"]/div/div/a/span");
	
	public SupplierPage(WebDriver Driver)
	{
		this.Driver = Driver;
		
	}
	
	public String VerifyPageDisplayed()
	{
		WebElement supplierPageText =   Driver.findElement(txtSupplierPage);
		supplierPageText.isDisplayed();
		return supplierPageText.getText();
	}
	
	
	

}
