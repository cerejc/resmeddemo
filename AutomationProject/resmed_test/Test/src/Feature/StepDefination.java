package Feature;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Common.Utility;
import Pages.GoogleHomePage;
import Pages.GoogleSearchResultPage;
import Pages.SupplierPage;
import Pages.WebSiteHomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefination  {
	

	public WebDriver Driver = null;

	public StepDefination () {
		
		try {
			String path = new File("").getCanonicalPath();
			System.setProperty("webdriver.chrome.driver", path + "\\drivers\\chromedriver.exe");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Driver = new ChromeDriver();	
	}
	
	@Given("^The user launches the web browser$")
	public void the_user_launches_the_web_browser() throws Throwable {
	
		String siteUrl = "https://www.google.com.au";

		Driver.get(siteUrl);
		Driver.manage().window().maximize();
	}
	
	@And("^The user searches the required website$")
	public void the_user_searches_the_required_website() throws Throwable {

		GoogleHomePage googleHomePage = new GoogleHomePage (Driver);
	
		googleHomePage.EnterText();
		googleHomePage.Click();
	
	}

	@When("^The user opens the searched website$")
	public void the_user_opens_the_searched_website() throws Throwable {
		
		GoogleSearchResultPage googleSearchResultPage = new GoogleSearchResultPage (Driver);
		googleSearchResultPage.ClickSearchResult();
	}

	@Then("^Verify that the correct website is opened$")
	public void verify_that_the_correct_website_is_opened() throws Throwable {
		
		WebSiteHomePage WebSiteHomePage = new WebSiteHomePage (Driver);
		WebSiteHomePage.verifyWebSiteHomePageisOpened();
		WebSiteHomePage.clickUserPreferenceDropdwn();
	}

	@And("^The user navigates to the required page$")
	public void the_user_navigates_to_the_required_page() throws Throwable {
		
		WebSiteHomePage webSiteHomePage = new WebSiteHomePage (Driver);
		webSiteHomePage.selectUserPreference();
 	}
	
	@Then("^Verify that the correct page displayed$")
	public void verify_that_the_login_details_are_displayed() throws Throwable {
		
		SupplierPage supplierPage = new SupplierPage(Driver);
		System.out.println("The required page: " + supplierPage.VerifyPageDisplayed() + " is displayed");
		    
		Driver.quit();
	}

}
