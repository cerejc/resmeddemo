Feature: Searching the Website

Scenario: Search website
Given The user launches the web browser
And The user searches the required website
When The user opens the searched website
Then Verify that the correct website is opened
And The user navigates to the required page 
Then Verify that the correct page displayed 

